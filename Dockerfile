FROM arm32v7/node:9

RUN mkdir /blinky

COPY . /blinky

WORKDIR /blinky

RUN npm install onoff

CMD ["node", "/blinky"]
