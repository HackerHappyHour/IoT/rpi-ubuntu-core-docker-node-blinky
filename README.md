# LED blink with RPi, Ubuntu Core, Docker, and Node.js

YouTube:[Node.js Blinky - Docker on Ubuntu Core on Raspberry Pi 3](https://youtu.be/BMJrgw8rgJ0)

Prerequisites:
- Raspberry Pi 2B/3 running [Ubuntu Core](https://www.ubuntu.com/core)
- [Basic understanding](https://medium.com/@vegiops/learn-docker-in-15-minutes-87c18cb84cbd) of [Docker](https://docker.com/what-container)
- Red LED
- 68 - 100 Ohm resistor
- Breadboard and Jumper Wires
- (Optional) Raspberry Pi T Cobbler & Ribbon Cable (~ $8 - $15)

## Setup

The first step immediately follows the last step of the [Getting Started (First Boot)](https://developer.ubuntu.com/core/get-started/raspberry-pi-2-3)
instructions for Ubuntu Core.

**Install Docker**

Ubuntu Core is pretty lean, lacking even `apt`. It _does_ however have `snap` (ugh), which gives you a quick and easy way to install docker.
`snap` hasn't been very popular apparently, because at the time of this writing, the official Docker snap installs `17.06` when the latest official
version is `17.12`. (2 stable releases since). But, it'll do for IoT projects, so i decided it was good enough.

```
sudo snap install docker
```

This will take a while, but should finish without problems. When you're done, verify docker is up and running using:

```
$ sudo docker version
Client:
  version: 17.06.2-ce
  blah
  blah
  blah

Server:
  version: 17.06.2-ce
  blah
  blah
  blah
```

**Install Node**

You'll want to use a version of node optimized for running on your Pi, for that, we'll pull:

```
sudo docker pull arm32v7/node:9
```

The final step in getting us to the same place most RPi tutorials start at, is running the docker image so that it has access to GPIO. There are two things required for this; mounting `/dev/memgpio` and `/sys` into your container. `/dev/memgpio` is a `--device`, and `/sys` is a `volume`. However, for ease of use, I'm just going to use
`--privileged` here:

```
sudo docker run -it --rm --privileged arm32v7/node:9 bash
root@54ac2d51ac:
```
At this point, if you're comfortable with `vi` or some other terminal editor, you can probably move forward on your own, following the tutorial from w3schools - [Node.js Raspberry Pi - GPIO Introduction](https://www.w3schools.com/nodejs/nodejs_raspberrypi_gpio_intro.asp)


## Blinky Blinky

You can follow the tutorial above if you choose to go your own, however if you just want to see that you have everything working right,
you can clone this project, rsync it to your pi, build your docker image, and run it.

```
# ssh into your rpi and create a ~/code/blinky directory
ssh yourubuntussousername@yourrpiIPAddress
$ mkdir -p code/blinky

# Now, from your primary machine (because there is no snap for `git`???!!!)
$ git clone git@gitlab.com:HackerHappyHour/IoT/rpi-ubuntu-core-docker-node-blinky.git
$ rsync -avz blinky/ yourubuntussousername@yourPiIPaddress:~/code/blinky
```

Now, assuming you've wired your LED up using pin 4 (G04) for GPIO, (see the [w3schools wiring diagram](https://www.w3schools.com/nodejs/img_raspberrypi3_led_simple.png) ), you can
run docker on your pi to build your image and blink your LED

```
# from your RPi
$ cd code/blinky
$ sudo docker build -t blinky .

# After build completes, make it blinks!
$ sudo docker run -it --rm --privileged -v `pwd`:/blinky blinky
# after short delay, should blink for 5 seconds then exit
```
